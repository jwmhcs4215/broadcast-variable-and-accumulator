import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.SparkContext._

val sc = new SparkContext("local", "Broadcast variable and Accumulator", new SparkConf())

val broadcast_var = sc.broadcast(Array(2, 4, 6, 8))

val acc = sc.accumulator(0, "accumulator")
sc.parallelize(broadcast_var.value).foreach(x => acc.add(x))
acc.value

sc.stop()
System.exit(0)